"""geopolis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from polis import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', view=views.index, name='index'),
    url(r'^save_faculty', view=views.save_faculty, name='save_faculty'),
    url(r'^save_groups', view=views.save_groups, name='save_groups'),
    url(r'', include('social_auth.urls')),
    url(r'^nodes', view=views.get_nodes, name='nodes'),
    url(r'^node', view=views.get_courses_by_node_id, name='courses')
]
