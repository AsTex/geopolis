# ==================================================
# Util
# ==================================================

polarToCartesian = (cx, cy, r, angle) ->
  angle = (angle - 90) * Math.PI / 180
  x: cx + r * Math.cos angle
  y: cy + r * Math.sin angle

# ==================================================
# Graph
# ==================================================

class Graph
  constructor: (@nodes) ->
    return

  children: (node) ->
    nodes = []
    for child in node.children
      nodes.push @nodes[child]
    console.log nodes
    return nodes

# ==================================================
# GUI
# ==================================================

class GUI
  constructor: (x, y, nodes) ->
    @paper = Snap window.innerWidth, window.innerHeight
    firstFive = nodes.slice 0, 5
    @root = new GraphNodes @, null, x, y, firstFive
    @graph = new Graph nodes

    do @_bindEvents

  # ================
  # Private
  # ================

  _bindEvents: ->
    window.addEventListener 'resize', =>
      @paper.attr
        width: window.innerWidth
        height: window.innerHeight




# ==================================================
# GraphNode
# ==================================================

class GraphNodes
  constructor: (@gui, @parent, x, y, @nodes) ->
    @size = 500
    @area = @gui.paper
      .svg x, y, @size, @size
      .addClass 'nav'
    @c = @size / 2 #center
    @r = @size * .25
    @angle = 360 / @nodes.length

    @container = do @area.g

    @updateNodes @nodes

# ================
# Private
# ================


# ================
# Public
# ================

  updateNodes: (nodes) ->
    do @container.clear
    for node, index in nodes
      _node = new Node(@, @angle * index, 100, node)
      @container.add do _node.svg

  fadeOut: ->
    @area
      .addClass 'fade'


# ==================================================
# Node
# ==================================================

class Node
  constructor: (@area, @angle, @R, @node) ->
    coordinate = polarToCartesian(@area.c, @area.c, @R, @angle)
    @x = coordinate.x
    @y = coordinate.y

  svg: ->
    @_group do @_node

  _node: ->
    @area.container
      .circle @x, @y, @node.weight
      .text @x, @y + @node.weight + 10, @node.title
      .addClass 'nav-node'

  _group: (child) ->
    @area.container
      .g child
      .mouseover => # add open node
        @_openNode @node

  _openNode: (node) ->
    do @area.fadeOut
    nav = new GraphNodes(@area.gui, @, @x - @area.c, @y - @area.c, @area.gui.graph.children(node))


# ==================================================
# Test
# ==================================================
nodes = [
  {
    name: 'base'
    title: 'Базовые знания'
    weight: 20
    children: [5, 6, 7]
  }
  {
    name: 'prof'
    title: 'Профессия'
    weight: 30
    children: [7, 8, 9]
  }
  {
    name: 'lifestyle'
    title: 'Образ жизни'
    weight: 10
    children: [5, 7, 9]
  }
  {
    name: 'relationship'
    title: 'Отношения'
    weight: 15
    children: [5, 6, 8, 9]
  }
  {
    name: 'culture'
    title: 'Культура'
    weight: 20
    children: [5, 6, 9]
  }
  {
    name: 'base'
    title: 'Базовые знания'
    weight: 20
    children: [5, 7, 8, 9]
  }
  {
    name: 'prof'
    title: 'Профессия'
    weight: 30
    children: []
  }
  {
    name: 'lifestyle'
    title: 'Образ жизни'
    weight: 10
    children: []
  }
  {
    name: 'relationship'
    title: 'Отношения'
    weight: 15
    children: []
  }
  {
    name: 'culture'
    title: 'Культура'
    weight: 20
    children: []
  }
]


gui = new GUI 0, 0, nodes
# do gui.nav.paper._node
# paper = Snap 800, 500

# style =
#   fill: '#887'
#   stroke: '#aaa'
#   strokeWidth: 20

# circle = paper
#   .circle 150, 150, 100
#   .attr style


# image = paper
#   .image 'assets/user.png', 100, 100, 100, 100
