(function(){
    var sys = arbor.ParticleSystem(100, 10000, 0.9);
    sys.screen({padding:[150, 150, 150, 150], step:.06});
    sys.parameters({gravity:false});
    sys.renderer = Renderer("#viewport") ;


    var data = {
        nodes:{
            user: {
                'color':'lightgray',
                'shape':'dot',
                'label':'ПОЛЬЗОВАТЕЛЬ'
            },
            science: {
                'color':'lightgreen',
                'shape':'dot',
                'label':'Наука и исследования'
            },
            tech: {
                'color':'lightblue',
                'shape':'dot',
                'label':'Технологии'
            },
            humanities: {
                'color':'lightcoral',
                'shape':'dot',
                'label':'Гуманитария'
            },
            service: {
                'color':'plum',
                'shape':'dot',
                'label':'Обслуживание'
            },
            manage: {
                'color':'lightsalmon',
                'shape':'dot',
                'label':'Управление'
            }
        },
        edges:{
            user:{
                science:{},
                tech:{},
                humanities:{},
                service:{},
                manage:{}
            }
        }
    };

    sys.graft(data);
    setTimeout(function(){
        var postLoadDataScience = {
            nodes:{
                physics: {
                    'color':'lightgreen',
                    'shape':'dot',
                    'label':'Физика'
                },
                math: {
                    'color':'lightgreen',
                    'shape':'dot',
                    'label':'Математика'
                },
                chem: {
                    'color':'lightgreen',
                    'shape':'dot',
                    'label':'Химия'
                }
            },
            edges:{
                science:{ physics:{},math:{},chem:{} }
            }
        };
        sys.graft(postLoadDataScience);
        }, 5000);

        setTimeout(function(){
            var postLoadDataScience = {
                nodes:{
                    al: {
                        'color':'lightgreen',
                        'shape':'dot',
                        'label':'Алгебра'
                    },
                    geo: {
                        'color':'lightgreen',
                        'shape':'dot',
                        'label':'Геометрия'
                    }
                },
                edges:{
                    math:{ al:{},geo:{} }
                }
            };
            sys.graft(postLoadDataScience);
        }, 15000);
})();