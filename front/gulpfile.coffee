gulp = require 'gulp'
coffee = require 'gulp-coffee'
connect = require 'gulp-connect'

gulp.task 'connect', ->
  connect.server
    port: 8000
    livereload: true

gulp.task 'coffee', ->
  gulp.src 'coffee/*.coffee'
    .pipe coffee bare: true
    .pipe gulp.dest 'js'
    .pipe do connect.reload

gulp.task 'snap', ->
  gulp.src 'Snap.svg/dist/*.js'
    .pipe gulp.dest 'js'

gulp.task 'watch', ->
  gulp.watch 'coffee/*.coffee', ['coffee']

gulp.task 'default', ['snap', 'coffee', 'connect', 'watch']