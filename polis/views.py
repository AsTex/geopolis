from django.core.serializers import json
from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.core import serializers

from .models import VKGroup, VKEducation, Node

import json

# Create your views here.
def index(request):
    return render(request,'login/login.html')
def verify(request):
    return HttpResponse(request.body)

def save_groups(request):
    result = {'status':'error', 'result':'use POST method'}
    if request.method == 'POST':
        groups = request.GET['group_ids']
        areas = {}
        for group in groups:
            d_group = VKGroup.objects.get(vk_id=group)
            count = areas.get(d_group.area)
            if count != None:
                areas[d_group.area] = count+1
        sorted(areas.items(), key=lambda x: x[1])
        result.update({'status':'ok', 'areas':areas.values()[0]})
        #areas now contains ordered by count our inner areas
    return HttpResponse(json.dumps(result))


def save_faculty(request):
    result = {'status':'error', 'result':'use POST method'}
    if request.method == 'POST':
        faculty = request.GET['faculty']
        area = VKEducation.objects.get(vk_faculty = faculty).area
        #area contains our inner area for better interest prediction
        result.update({'status':'ok', 'area':area})
    return HttpResponse(json.dumps(result))


def get_nodes(request):
    if request.method == 'GET':
        nodes = Node.objects.all()

        for node in nodes:
            # node.children_indices = []
            # node.sibling_indices = []
            # Get weight from direct interests)]
            int_node = node.interested_in(request.GET['player_id'])
            if len(int_node) > 0:
                node.weight = int_node.all()[0].weight
            else:
                node.weight = 0

        # Please forgive me for cubic time here : (
        for i in range(len(nodes)):
            for j in range(i + 1, len(nodes)):
                for child in nodes[i].children.all():
                    if child.id == nodes[j].id:
                        nodes[i].children_indices.append(j)
                        weightToAdd = child.interested_in(request.GET['player_id'])
                        assert(len(weightToAdd) < 2)
                        if len(weightToAdd) == 1:
                            nodes[i].weight += child.interested_in(request.GET['player_id']).all()[0].weight

        # Please forgive me for cubic time here : (
        for i in range(len(nodes)):
            for j in range(len(nodes)):
                if i == j:
                    continue
                for sibling in nodes[i].siblings.all():
                    if sibling.id == nodes[j].id:
                        nodes[i].sibling_indices.append(j)
                        nodes[j].sibling_indices.append(i)

        data = json.dumps([node.as_json() for node in nodes])
        return HttpResponse(data, content_type='application/json')


def get_courses_by_node_id(request):
    if request.method == 'GET':
        node = Node.objects.get(id=request.GET['id'])
        print node
        return HttpResponse(serializers.serialize('json', node.course_set.all()), content_type='application/json')

