from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class Node(models.Model):
    name = models.CharField(max_length=255)      # global name
    title = models.CharField(max_length=255)     # localized name
    description = models.CharField(max_length=1023)
    color = models.CharField(max_length=32)
    children = models.ManyToManyField('self', through='ParentOf', symmetrical=False, related_name='NodeChildren')
    siblings = models.ManyToManyField('self', through='SiblingOf', symmetrical=False, related_name='NodeSib')
    courses = models.ManyToManyField('Course')
    size = models.IntegerField(default=5)  # from 1 to infinity
    radius = models.IntegerField()         # how one node is close to another, I guess for pres it'd be random

    def as_json(self):
        return dict(
            id=self.id, name=self.name,
            title=self.title,
            description=self.description,
            weight=self.weight,
            children_indices=self.children_indices,
            sibling_indices=self.sibling_indices,
            color=self.color,
            size=self.size)

    def interested_in(self, player_id):
        return InterestedIn.objects.filter(node=self.id, player=player_id)

    def __init__(self, *args, **kwargs):
        super(Node, self).__init__(*args, **kwargs)
        self.children_indices = []
        self.sibling_indices = []

    def __str__(self):
        return self.name


class ParentOf(models.Model):
    parent_node = models.ForeignKey(Node, related_name='parent')
    child_node = models.ForeignKey(Node, related_name='child')


class SiblingOf(models.Model):
    sibling_1 = models.ForeignKey(Node, related_name='sibling_1')
    sibling_2 = models.ForeignKey(Node, related_name='sibling_2')


class Player(models.Model):
    """Entity"""
    user = models.OneToOneField(User)
    interested_id = models.ManyToManyField(Node)

    def __str__(self):
        return "{0}".format(self.user)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = Player.objects.get_or_create(user=instance)


post_save.connect(create_user_profile, sender=User)


class Keyword(models.Model):
    """Entity """
    name = models.CharField(max_length=255)


class VKGroup(models.Model):
    """Entity """
    vk_id = models.CharField(max_length=32)
    area = models.CharField(max_length=255)
    keywords = models.ManyToManyField(Keyword)
    member_of = models.ManyToManyField(Player)


class VKEducation(models.Model):
    """Entity """
    vk_faculty = models.CharField(max_length=255)
    area = models.CharField(max_length=255)
    keywords = models.ManyToManyField(Keyword)


class Course(models.Model):
    """Entity """
    name = models.CharField(max_length=255)
    description = models.TextField(blank=False)
    complexity = models.IntegerField(default=5)
    rating = models.IntegerField(default=0)
    keywords = models.ManyToManyField(Keyword)
    nodes = models.ManyToManyField(Node)
    take = models.ManyToManyField(Player)

    def __str__(self):
        return self.name


class Takes(models.Model):
    """Relationship data for Player-Course"""
    player = models.ForeignKey(Player)
    course = models.ForeignKey(Course)
    progress = models.IntegerField(default=0)


class InterestedIn(models.Model):
    player = models.ForeignKey(Player)
    node = models.ForeignKey(Node)

    # real values is for 0 to 10. Zero when user isn't
    # interested at all, so it is default
    weight = models.IntegerField(default=0)



