# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('polis', '0002_auto_20151107_2101'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('complexity', models.IntegerField(default=5)),
                ('rating', models.IntegerField(default=0)),
                ('keywords', models.ManyToManyField(to='polis.Keyword')),
            ],
        ),
        migrations.CreateModel(
            name='InterestedIn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weight', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=1023)),
                ('color', models.CharField(max_length=32)),
                ('size', models.IntegerField(default=5)),
                ('radius', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ParentOf',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('child_node', models.ForeignKey(related_name='child', to='polis.Node')),
                ('parent_node', models.ForeignKey(related_name='parent', to='polis.Node')),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('interested_id', models.ManyToManyField(to='polis.Node')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SiblingOf',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sibling_1', models.ForeignKey(related_name='sibling_1', to='polis.Node')),
                ('sibling_2', models.ForeignKey(related_name='sibling_2', to='polis.Node')),
            ],
        ),
        migrations.CreateModel(
            name='Takes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('progress', models.IntegerField(default=0)),
                ('course', models.ForeignKey(to='polis.Course')),
                ('player', models.ForeignKey(to='polis.Player')),
            ],
        ),
        migrations.AddField(
            model_name='node',
            name='children',
            field=models.ManyToManyField(related_name='NodeChildren', through='polis.ParentOf', to='polis.Node'),
        ),
        migrations.AddField(
            model_name='node',
            name='courses',
            field=models.ManyToManyField(to='polis.Course'),
        ),
        migrations.AddField(
            model_name='node',
            name='siblings',
            field=models.ManyToManyField(related_name='NodeSib', through='polis.SiblingOf', to='polis.Node'),
        ),
        migrations.AddField(
            model_name='interestedin',
            name='node',
            field=models.ForeignKey(to='polis.Node'),
        ),
        migrations.AddField(
            model_name='interestedin',
            name='player',
            field=models.ForeignKey(to='polis.Player'),
        ),
        migrations.AddField(
            model_name='course',
            name='nodes',
            field=models.ManyToManyField(to='polis.Node'),
        ),
        migrations.AddField(
            model_name='course',
            name='take',
            field=models.ManyToManyField(to='polis.Player'),
        ),
        migrations.AddField(
            model_name='vkgroup',
            name='member_of',
            field=models.ManyToManyField(to='polis.Player'),
        ),
    ]
