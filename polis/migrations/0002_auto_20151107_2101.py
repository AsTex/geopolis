# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='vkeducation',
            name='keywords',
            field=models.ManyToManyField(to='polis.Keyword'),
        ),
        migrations.AddField(
            model_name='vkgroup',
            name='keywords',
            field=models.ManyToManyField(to='polis.Keyword'),
        ),
    ]
