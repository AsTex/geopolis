# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polis', '0003_auto_20151108_0200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vkgroup',
            name='vk_id',
            field=models.CharField(max_length=32),
        ),
    ]
